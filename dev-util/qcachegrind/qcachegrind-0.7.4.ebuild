# Distributed under the terms of the GNU General Public License v2

EAPI=4

inherit qt4-r2

MY_P="kcachegrind-${PV}"
S="${WORKDIR}/${MY_P}"

DESCRIPTION="Qt4 based callgrind visualiser"
HOMEPAGE="http://kcachegrind.sourceforge.net/"
SRC_URI="http://kcachegrind.sourceforge.net/${MY_P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND=">=dev-qt/qtgui-4.5.0"


src_install() {
	into /usr
	dobin qcachegrind/qcachegrind
	insinto /usr/share/applications
	doins qcachegrind/qcachegrind.desktop
	mv kcachegrind/hi32-app-kcachegrind.png kcachegrind.png
	insinto /usr/share/icons/hicolor/32x32/apps/
	doins kcachegrind.png
	mv kcachegrind/hi48-app-kcachegrind.png kcachegrind.png
	insinto /usr/share/icons/hicolor/48x48/apps/
	doins kcachegrind.png
}

pkg_postinst() {
	elog "Emerge media-gfx/graphviz to be able to view call graphs."
}
