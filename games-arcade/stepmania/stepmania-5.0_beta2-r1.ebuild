# Distributed under the terms of the GNU General Public License v2

EAPI="4"

# This is actually version beta2a, but gentoo doesn't allow such names

inherit autotools eutils flag-o-matic gnome2-utils games mercurial

DESCRIPTION="A cross-platform rhythm video game."
HOMEPAGE="http://www.stepmania.com/"
LICENSE="MIT"
SLOT="0"
KEYWORDS="~x86 ~amd64"

EHG_REPO_URI="http://code.google.com/p/stepmania/"
EHG_REVISION="SM5-beta2a"

# This is to prevent 414 error on google code
# Also prevents too much pulling
EHG_CLONE_CMD="${EHG_CLONE_CMD} -b default"
EHG_PULL_CMD="${EHG_PULL_CMD} -b default"

IUSE="alsa custom-cflags +gtk +ffmpeg gles2 +jpeg +mp3 +network oss profiling pulseaudio static-ffmpeg static-glew static-jpeg static-png static-vorbis sse2 +vorbis X debug"

RDEPEND="app-arch/bzip2
		dev-libs/libpcre
		>=media-libs/glew-1.3.5
		media-libs/libpng
		sys-libs/zlib
		virtual/opengl
		virtual/glu
		x11-libs/gtk+:3
		alsa? ( >=media-libs/alsa-lib-0.9.0 )
		ffmpeg? ( || ( media-video/ffmpeg media-video/libav ) )
		jpeg? ( virtual/jpeg )
		mp3? ( media-libs/libmad )
		oss? ( media-sound/oss )
		pulseaudio? ( media-sound/pulseaudio )
		static-ffmpeg? ( || ( media-video/ffmpeg[static-libs] media-video/libav[static-libs] ) )
		static-glew? ( >=media-libs/glew-1.3.5[static-libs] )
		static-jpeg? ( virtual/jpeg[static-libs] )
		static-png? ( media-libs/libpng[static-libs] )
		static-vorbis? ( media-libs/libvorbis[static-libs] )
		vorbis? ( media-libs/libvorbis )
		X? ( x11-base/xorg-server
			 x11-libs/libXtst
			 x11-libs/libXrandr )"

DEPEND="${RDEPEND}
		dev-vcs/mercurial
		virtual/pkgconfig"

REQUIRED_USE="static-ffmpeg? ( ffmpeg )
			static-jpeg? ( jpeg )
			static-vorbis? ( vorbis )
			|| ( pulseaudio alsa oss )"

WANT_LIBTOOL="none"
AT_NOELIBTOOLIZE="yes"

src_prepare() {
	epatch "${FILESDIR}/linux.patch"
	eautoreconf
}

src_configure() {
	if ! use custom-cflags; then
		filter-flags -O0 -O1 -O2 -O3
		append-flags -O3
	fi

	egamesconf \
			--disable-dependency-tracking \
			--disable-maintainer-mode \
			--datarootdir=/usr/share \
			--enable-fhs \
			$(use_with alsa) \
			$(use_with gtk gtk3) \
			$(use_with ffmpeg) \
			$(use_with gles2) \
			$(use_with jpeg) \
			$(use_with mp3) \
			$(use_with network) \
			$(use_with oss) \
			$(use_with profiling prof) \
			$(use_with pulseaudio) \
			$(use_with static-ffmpeg) \
			$(use_with static-glew) \
			$(use_with static-jpeg) \
			$(use_with static-png) \
			$(use_with static-vorbis)
			$(use_with sse2) \
			$(use_with vorbis) \
			$(use_with X x) \
			$(use_with debug)
}

src_install() {
	emake DESTDIR="${D}" install

	prepgamesdirs
}

pkg_postinst() {
	gnome2_icon_cache_update
	games_pkg_postinst

	if ! use jpeg; then
		ewarn "You have disabled jpeg support."
		ewarn "Expect lots of missing graphics."
	fi
	elog "You can put all your songs, themes etc. into"
	elog "~/.stepmania-5.0/ as if it were normal stepmania directory."
	elog "Run stepmania once to create it."
}

pkg_postrm() {
	gnome2_icon_cache_update
}
